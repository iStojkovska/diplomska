//
//  User.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/6/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation

enum UserType: String {
    case sender = "sender"
    case deliverer = "deliverer"
}

struct PreferredDate: Codable {
    var start: String = ""
    var end: String = ""
    
    init() {}
    
    enum CodingKeys: String, CodingKey {
        case start
        case end
    }
}

class User: Codable {
    
    static let sharedInstance: User = User()
    
    private var id: Int = 0
    private var email: String = ""
    private var password: String = ""
    private var fullName: String = ""
    private var type: String = ""
    private var prefferedSize: String?
    private var prefferedDate: PreferredDate?
    
    enum CodingKeys: String, CodingKey {
        case id
        case email
        case fullName
        case type
        case prefferedSize
        case prefferedDate
    }
    
    public func updateUser(user: User) {
        self.id = user.id
        self.email = user.email
        self.password = user.password
        self.fullName = user.fullName
        self.type = user.type
        if let size = user.prefferedSize {
            self.prefferedSize = size
        }
        if let date = user.prefferedDate {
            self.prefferedDate = date
        }
    }
    
    //GETTERS
    public func getUserId() -> Int {
        return self.id
    }
    public func getEmail() -> String {
        return self.email
    }
    public func getPassword() -> String {
        return self.password
    }
    public func getFullName() -> String {
        return self.fullName
    }
    public func getType() -> UserType {
        if self.type == "sender" {
            return .sender
        } else {
            return .deliverer
        }
    }
    public func getPreferredSize() -> String {
        if self.prefferedSize != nil {
            return self.prefferedSize!
        } else {
            return ""
        }
    }
    public func getPreferredDate() -> PreferredDate? {
        return self.prefferedDate
    }
    
    //SETTERS
    public func setUserId(id: Int) {
        self.id = id
    }
    public func setEmail(email: String) {
        self.email = email
    }
    public func setPassword(password: String) {
        self.password = password
    }
    public func setFullName(fullName: String) {
        self.fullName = fullName
    }
    public func setType(userType: UserType) {
        self.type = userType.rawValue
    }
    public func setPreferredSize(size: String) {
        self.prefferedSize = size
    }
    public func setPreferredDateStart(start: Int64) {
        if self.prefferedDate == nil {
            self.prefferedDate = PreferredDate()
        }
        self.prefferedDate?.start = "\(start)"
    }
    public func setPreferredDateEnd(end: Int64) {
        if self.prefferedDate == nil {
            self.prefferedDate = PreferredDate()
        }
        self.prefferedDate?.end = "\(end)"
    }
    
    
    
}
