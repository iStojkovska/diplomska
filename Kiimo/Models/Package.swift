//
//  Package.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/6/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation

enum ExchangeType: String {
    case call = "call"
    case waitOutside = "waitOutside"
    case reception = "reception"
    case other = "other"
}

struct Location {
    var start: String = ""
    var end: String = ""

}

struct PackageInTransit: Codable {
    var packages: [Package] = []
    var codes: [Int: String] = [:]
    
    enum CodingKeys: String, CodingKey {
        case packages
        case codes
    }
    
    init() {}
}

class Package: Codable {

    private var id: Int = 0
    private var startLocation: Location = Location()
    private var endLocation: Location = Location()
    private var startAddress: String = ""
    private var endAddress: String = ""
    private var name: String = ""
    private var price: String = ""
    private var senderId: String = ""
    private var receiverName: String = ""
    private var receiverPhone: String = ""
    private var comments: String = ""
    private var deliverDate: String = ""
    private var size: String = ""
    private var additinalInfo: String = ""
    private var exchangeType:  String = ""
    private var status: String = ""
    private var imageUrl: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case startAddress
        case endAddress
        case name
        case price
        case senderId
        case receiverName
        case receiverPhone
        case comments
        case deliverDate
        case size
        case additinalInfo
        case exchangeType
    }
    
    //GETTERS
    public func getId() -> Int {
        return self.id
    }
    public func getStartLocation() -> Location {
        return self.startLocation
    }
    public func getEndLocation() -> Location {
        return self.endLocation
    }
    public func getStartAddress() -> String {
        return self.startAddress
    }
    public func getEndAddress() -> String {
        return self.endAddress
    }
    public func getName() -> String {
        return self.name
    }
    public func getPrice() -> String {
        return self.price
    }
    public func getSenderId() -> String {
        return self.senderId
    }
    public func getReceiverName() -> String {
        return self.receiverName
    }
    public func getReceiverPhone() -> String {
        return self.receiverPhone
    }
    public func getComments() -> String {
        return self.comments
    }
    public func getDeliverDate() -> Int64 {
        return Int64(self.deliverDate)!
    }
    public func getSize() -> String {
        return self.size
    }
    public func getAdditinalInfo() -> String {
        return self.additinalInfo
    }
    public func getExchangeType() -> ExchangeType {
        return ExchangeType(rawValue: self.exchangeType)!
    }
    public func getStatus() -> String {
        return self.status
    }
    public func getImageUrl() -> String {
        return self.imageUrl
    }
    
    //SETTERS
    public func setId(id: Int) {
        self.id = id
    }
    public func setStartLocation(startLocation: Location) {
        self.startLocation = startLocation
    }
    public func setEndLocation(endLocation: Location) {
        self.endLocation = endLocation
    }
    public func setStartAddress(startAddress: String) {
        self.startAddress = startAddress
    }
    public func setEndAddress(endAddress: String) {
        self.endAddress = endAddress
    }
    public func setName(name: String) {
        self.name = name
    }
    public func setPrice(price: String) {
        self.price = price
    }
    public func setSenderId(senderId: String) {
        self.senderId = senderId
    }
    public func setReceiverName(receiverName: String) {
        self.receiverName = receiverName
    }
    public func setReceiverPhone(receiverPhone: String) {
        self.receiverPhone = receiverPhone
    }
    public func setComments(comments: String) {
        self.comments = comments
    }
    public func setDeliverDate(deliverDate: Int64) {
        self.deliverDate = "\(deliverDate)"
    }
    public func setSize(size: String) {
        self.size = size
    }
    public func setAdditinalInfo(additinalInfo: String) {
        self.additinalInfo = additinalInfo
    }
    public func setExchangeType(exchangeType: ExchangeType) {
        self.exchangeType = exchangeType.rawValue
    }
    public func setStatus(status: String) {
        self.status = status
    }
    public func setImageUrl(imageUrl: String) {
        self.imageUrl = imageUrl
    }
}
