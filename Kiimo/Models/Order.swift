//
//  Order.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/6/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation


class Order {
    
    private var id: String = ""
    private var packageId: String = ""
    private var code: String = ""
    private var confirmed: Bool = false
    private var rating: Double = 0.0
    private var comment: String = ""
    private var delivererId: String = ""

}
