//
//  InTransitPackagesViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/17/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class InTransitPackagesViewController: UIViewController {
    
    @IBOutlet weak var topNavigatonBar: NavigationBar!
    @IBOutlet weak var errorLabel: UILabel!
    
    private var scrollView: UIScrollView = UIScrollView()
    private var packagesInTransit: PackageInTransit = PackageInTransit()
    private var selectedPackageId: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollView()
        errorLabel.layer.opacity = 0
        getPackagesInTransit()
    }
    
    private func initScrollView() {
        scrollView = UIScrollView(frame: CGRect(x: 0, y: topNavigatonBar.frame.height, width: self.view.frame.width, height: self.view.frame.height - topNavigatonBar.frame.height))
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat(packagesInTransit.packages.count * 82))
        scrollView.showsVerticalScrollIndicator = false
        scrollView.bounces = false
        self.view.addSubview(scrollView)
    }
    
    private func getPackagesInTransit() {
        NetworkManager.sharedInstance.getInTransitPackages(id: User.sharedInstance.getUserId()) { (packagesInTransit, error) in
            
            if let packages = packagesInTransit?.packages {
                if packages.count > 0 {
                    self.packagesInTransit = packagesInTransit!
                    self.scrollView.translatesAutoresizingMaskIntoConstraints = true
                    self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat(packages.count * 82))
                    
                    var originY: CGFloat = 0
                    for i in 0 ..< packages.count {
                        let packageView: PackageView = PackageView(frame: CGRect(x: 0, y: originY, width: self.view.frame.width, height: 88), package: packages[i])
                        packageView.tag = i
                        packageView.delegate = self
                        originY += 88
                        self.scrollView.addSubview(packageView)
                    }
                    UIManagmentService.sharedInstance.animateLabels(viewTag: 0, view: self.scrollView)
                } else {
                    UIManagmentService.sharedInstance.showErrorLabel(type: .updateError, errorLabel: self.errorLabel, text: nil)
                }
            } else {
                guard let err = error else {
                    print("Cannot get optional value of error")
                    return
                }
                if let statusCode = err["statusCode"], let errMessage = err["errMsg"] {
                    print("=====CANNOT GET ALL PACKAGES IN TRANSIT ========")
                    print("WITH STATUS CODE \(statusCode) AND MESSAGE \(errMessage)")
                }
                UIManagmentService.sharedInstance.showErrorLabel(type: .showError, errorLabel: self.errorLabel, text: nil)
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "inTransitPackageDetailsSegue" {
            guard let index = packagesInTransit.packages.index(where: {$0.getId() == selectedPackageId}) else {
                    print("Cannot be found package with this ID in inTransitPackagesList")
                    return
            }
            //TODO: DA MU KAZAM NA BRANE DA SMENI VO BAZA VO CODES KEY DA VRAKJA PACKAGE ID, NE ORDER ID
            if let packageDetailsVC = segue.destination as? PackageDetailsViewController {
                packageDetailsVC.setSelectedPackageID(id: selectedPackageId, code: nil)
            }
            
        }
    }
    
}

extension InTransitPackagesViewController: PackageViewDelegate {
    func packageTapped(id: Int) {
        selectedPackageId = id
        performSegue(withIdentifier: "inTransitPackageDetailsSegue", sender: nil)
    }
    
    
}
