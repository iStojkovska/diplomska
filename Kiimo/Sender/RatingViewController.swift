//
//  RatingViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 12/6/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit
import Cosmos

protocol RatingViewControllerDelegate: class {
    func dismissVC()
}

class RatingViewController: UIViewController {
    
    @IBOutlet weak var code1TextField: UITextField!
    @IBOutlet weak var code2TextField: UITextField!
    @IBOutlet weak var code4TextField: UITextField!
    @IBOutlet weak var code3TextField: UITextField!
    
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    
    private var ratingFinal: Int = 0
    private var packageId: Int!
    
    weak var delegate: RatingViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView(){
        finishButton.layer.cornerRadius = 10
        finishButton.titleLabel?.font = UIFont.bold16
        disableButton()
        
        commentTextView.delegate = self
        commentTextView.textColor = UIColor.lightGray
        
        code1TextField.delegate = self
        code2TextField.delegate = self
        code3TextField.delegate = self
        code4TextField.delegate = self
        
        ratingView.didTouchCosmos = {
            rating in
            self.ratingFinal = Int(rating)
            
            if self.ratingFinal > 0 && !self.commentTextView.text.isEmpty && self.checkCodeTextFieldsInput() {
                self.enableButton()
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func checkCodeTextFieldsInput() -> Bool {
        if (code1TextField.text?.isEmpty)! {
            return false
        } else if (code2TextField.text?.isEmpty)! {
            return false
        } else if (code3TextField.text?.isEmpty)! {
            return false
        } else if (code4TextField.text?.isEmpty)! {
            return false
        } else {
            return true
        }
    }
    
    private func disableButton() {
        UIView.animate(withDuration: 0.2, animations: {
            self.finishButton.layer.opacity = 0.4
        }) { (_) in
            self.finishButton.isEnabled = false
        }
    }
    
    private func enableButton() {
        UIView.animate(withDuration: 0.2, animations: {
            self.finishButton.layer.opacity = 1
        }) { (_) in
            self.finishButton.isEnabled = true
        }
    }
    
    public func setPackage(id: Int) {
        self.packageId = id
    }
    
    @IBAction func didEndEditing(_ sender: UITextField) {
        if checkCodeTextFieldsInput() && !commentTextView.text.isEmpty && ratingFinal > 0 {
            enableButton()
        }
    }
    
    
    
    @IBAction func finishButtonTapped(_ sender: UIButton) {
        let confirmationCode: String = "\(code1TextField.text!)\(code2TextField.text!)\(code3TextField.text!)\(code4TextField.text!)"
        NetworkManager.sharedInstance.packageDelivered(id: packageId, confirmationCode: confirmationCode, rating: ratingFinal, comment: commentTextView.text) { (response, error) in
            if response {
                print("======== PAKCAGE DELIVERED ==========")
                self.dismiss(animated: true, completion: nil)
                self.delegate?.dismissVC()
            } else {
                guard let err = error else {
                    print("Cannot get optional value of error")
                    return
                }
                if let statusCode = err["statusCode"], let errMessage = err["errMsg"] {
                    print("=====CANNOT GET PACKAGE DETAILS ========")
                    print("WITH STATUS CODE \(statusCode) AND MESSAGE \(errMessage)")
                }
            }
        }
        
    }
    
}

extension RatingViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add comment"
            textView.textColor = UIColor.lightGray
        } else {
            if checkCodeTextFieldsInput() && ratingFinal > 0 {
                enableButton()
            }
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7300000191)
        }
    }
}

extension RatingViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 1
    }
}
