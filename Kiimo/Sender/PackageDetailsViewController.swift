//
//  PackageDetailsViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 12/2/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

protocol PackageDetailsViewControllerDelegate: class {
    func bidButtonTapped()
}

class PackageDetailsViewController: UIViewController {
    
    @IBOutlet weak var packageImageView: UIImageView!
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var packagePriceLabel: UILabel!
    @IBOutlet weak var packagePickupLabel: UILabel!
    @IBOutlet weak var packageDeliverLabel: UILabel!
    @IBOutlet weak var packageTimeLabel: UILabel!
    @IBOutlet weak var packageSizeLabel: UILabel!
    @IBOutlet weak var packageExchangeTypeLabel: UILabel!
    @IBOutlet weak var packageAddtionalInfoLabel: UILabel!
    @IBOutlet weak var packageRecieverNameLabel: UILabel!
    @IBOutlet weak var packageRecieverPhoneLabel: UILabel!
    @IBOutlet weak var packageConfirmationCodeLabel: UILabel!
    
    @IBOutlet weak var packageDetailsStackView: UIStackView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var bidButton: UIButton!
    
    private var selectedPackageId: Int!
    private var code: String?
    private var package: Package = Package()
    
    var buttonTitle: String?
    weak var delegate: PackageDetailsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
        getPackageDetails()
    }
    
    private func initLayout() {
        if User.sharedInstance.getType() == .sender {
            bidButton.heightAnchor.constraint(equalToConstant: 0).isActive = true
        } else {
            if buttonTitle != nil {
                bidButton.setTitle(buttonTitle!, for: .normal)
            }
        }
        errorLabel.layer.opacity = 0
        packageDetailsStackView.layer.opacity = 0
        packageConfirmationCodeLabel.layer.opacity = 0
    }
    
    private func getPackageDetails() {
        NetworkManager.sharedInstance.getPackageDetails(id: selectedPackageId) { (package, error) in
            if let selectedPackage = package {
                self.package = selectedPackage
                if selectedPackage.getImageUrl().isEmpty {
                    self.packageImageView.image = #imageLiteral(resourceName: "recieverName")
                } else {
                    self.packageImageView.sd_setImage(with: URL(string: selectedPackage.getImageUrl()), placeholderImage: #imageLiteral(resourceName: "activePachages"), options: [], completed: nil)
                }
                self.packageNameLabel.text = selectedPackage.getName()
                self.packagePriceLabel.text = selectedPackage.getPrice()
                self.packagePickupLabel.text = selectedPackage.getStartAddress()
                self.packageDeliverLabel.text = selectedPackage.getEndAddress()
                self.packageTimeLabel.text = UIManagmentService.sharedInstance.getDeliverTimeFormat(deliverDate: selectedPackage.getDeliverDate())
                self.packageSizeLabel.text = selectedPackage.getSize()
                self.packageExchangeTypeLabel.text = UIManagmentService.sharedInstance.getExchangeTimeFormat(exchangeType: selectedPackage.getExchangeType())
                self.packageAddtionalInfoLabel.text = selectedPackage.getAdditinalInfo()
                self.packageRecieverNameLabel.text = selectedPackage.getReceiverName()
                self.packageRecieverPhoneLabel.text = selectedPackage.getReceiverPhone()
                UIView.animate(withDuration: 0.2, animations: {
                    if let confirmationCode = self.code {
                        self.packageConfirmationCodeLabel.text = confirmationCode
                    }
                }, completion: { (_) in
                    if self.packageDetailsStackView.layer.opacity == 0 {
                        self.packageDetailsStackView.layer.opacity = 1
                        if self.code != nil {
                            self.packageConfirmationCodeLabel.layer.opacity = 1
                        }
                    }
                })
            } else {
                guard let err = error else {
                    print("Cannot get optional value of error")
                    return
                }
                if let statusCode = err["statusCode"], let errMessage = err["errMsg"] {
                    print("=====CANNOT GET PACKAGE DETAILS ========")
                    print("WITH STATUS CODE \(statusCode) AND MESSAGE \(errMessage)")
                }
                UIManagmentService.sharedInstance.showErrorLabel(type: .updateError, errorLabel: self.errorLabel, text: nil)
            }
        }
    }
    
    public func setSelectedPackageID(id: Int, code: String?) {
        self.selectedPackageId = id
        self.code = code
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ratingSegue" {
            if let ratingVC = segue.destination as? RatingViewController {
                ratingVC.delegate = self
                ratingVC.setPackage(id: selectedPackageId)
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func mainButtonTapped(_ sender: UIButton) {
        if bidButton.title(for: .normal) == "BID" {
            
            NetworkManager.sharedInstance.acceptPackage(delivererId: User.sharedInstance.getUserId(), packageId: selectedPackageId, senderId: package.getSenderId()) { (code, error) in
                if code != nil {
                    self.delegate?.bidButtonTapped()
                    self.dismiss(animated: true, completion: nil)
                } else {
                    guard let err = error else {
                        print("Cannot get optional value of error")
                        return
                    }
                    if let statusCode = err["statusCode"], let errMessage = err["errMsg"] {
                        print("===== DELIVERER CANNOT ACCEPT PACKAGE  ========")
                        print("WITH STATUS CODE \(statusCode) AND MESSAGE \(errMessage)")
                    }
                }
            }
        } else if bidButton.title(for: .normal) == "DELIVER" {
            performSegue(withIdentifier: "ratingSegue", sender: nil)
        }
    }
}

extension PackageDetailsViewController: RatingViewControllerDelegate {
    func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
}
