//
//  NewOrder1ViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/18/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit



class NewOrder1ViewController: UIViewController {

    @IBOutlet weak var takeaPhotoButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var packageNameTextField: UITextField!
    @IBOutlet weak var deliverDateTextField: UITextField!
    
    @IBOutlet weak var sOptionButton: SSRadioButton!
    @IBOutlet weak var mOptionButton: SSRadioButton!
    @IBOutlet weak var lOptionButton: SSRadioButton!
    
    private var controllerPackage: Package = Package()
    
    private var selectedTextField: UITextField = UITextField()
    private var radioButtonController: SSRadioButtonsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        packageNameTextField.delegate = self
        
        radioButtonController = SSRadioButtonsController(buttons: sOptionButton, mOptionButton, lOptionButton)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let pageViewController = self.parent as? PageViewController else { return }
        guard let nextVC = pageViewController.getViewController(index: 1) as? NewOrder2ViewController else { return }
        nextVC.controllerPackage = self.controllerPackage
    }
    
    private func initView() {
        takeaPhotoButton.layer.cornerRadius = 10
        galleryButton.layer.cornerRadius = 10
        takeaPhotoButton.titleLabel?.font = UIFont.bold16
        galleryButton.titleLabel?.font = UIFont.bold16
        
        packageNameTextField.layer.cornerRadius = 10
        deliverDateTextField.layer.cornerRadius = 10
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc private func datePickerChanged (datePicker: UIDatePicker) {
        
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "MMM dd, HH:mm"
        selectedTextField.text = formatter.string(from: datePicker.date)
        controllerPackage.setDeliverDate(deliverDate: Int64((datePicker.date.timeIntervalSince1970).rounded()))
    }
    
    @IBAction func calendarOpen(_ sender: UITextField) {
        let datePicker: UIDatePicker = UIDatePicker()
        selectedTextField = sender
        sender.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePickerChanged(datePicker:)), for: .valueChanged)
    }
    
    
    @IBAction func nameTextFieldDidEndEditing(_ sender: UITextField) {
        if !(sender.text?.isEmpty)! {
            controllerPackage.setName(name: sender.text!)
        }
        
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        guard let pageViewController = self.parent as? PageViewController else { return }
        pageViewController.dismiss(animated: true, completion: nil)
    }
    
}

extension NewOrder1ViewController: UITextFieldDelegate {
    
}

extension NewOrder1ViewController: SSRadioButtonControllerDelegate {
    func didSelectButton(selectedButton: UIButton?) {
        if selectedButton?.tag == 1 {
            controllerPackage.setSize(size: "s")
        } else if selectedButton?.tag == 2 {
            controllerPackage.setSize(size: "m")
        } else if selectedButton?.tag == 3 {
            controllerPackage.setSize(size: "l")
        }
    }
    
    
}
