//
//  NewOrder4ViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/18/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class NewOrder4ViewController: UIViewController {
    
    @IBOutlet weak var recieverNameTextField: UITextField!
    @IBOutlet weak var additionalInfoTextView: UITextView!
    @IBOutlet weak var recieverPhoneTextField: UITextField!
    @IBOutlet weak var createPackageButton: UIButton!
    
    private var selectedTextView: UITextView = UITextView()
    private var textFieldSelected: Bool = false
    private var viewMoved: CGFloat = 0.0
    
    var controllerPackage: Package!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func initView() {
        recieverNameTextField.layer.cornerRadius = 10
        recieverPhoneTextField.layer.cornerRadius = 10
        additionalInfoTextView.layer.cornerRadius = 10
        
        recieverNameTextField.delegate = self
        recieverPhoneTextField.delegate = self
        additionalInfoTextView.delegate = self
        
        selectedTextView = additionalInfoTextView
        createPackageButton.layer.cornerRadius = 10
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            if self.view.frame.origin.y == 0 {
                if !textFieldSelected && (selectedTextView.frame.maxY + 16 > frame.origin.y)  {
                    viewMoved = selectedTextView.frame.maxY + 16 - frame.origin.y
                    self.view.frame.origin.y -= viewMoved
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y += viewMoved
        }
    }
    
    @IBAction func createPackageButtonTapped(_ sender: UIButton) {
        NetworkManager.sharedInstance.createPackage(package: controllerPackage) { (response, error) in
            if response {
                guard let pageViewController = self.parent as? PageViewController else { return }
                pageViewController.packageDelegate?.newPackageAdded(package: self.controllerPackage)
                pageViewController.dismiss(animated: true, completion: nil)
            } else {
                guard let err = error else {
                    print("Cannot get error value")
                    return
                }
                if let statusCode = err["statusCode"], let errMessage = err["errMsg"] {
                    print("=====CANNOT CREATE A NEW PACKAGE ========")
                    print("WITH STATUS CODE \(statusCode) AND MESSAGE \(errMessage)")
                }
            }
        }
    }
}

extension NewOrder4ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textFieldSelected = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        guard let text = textField.text else { return }
        if textField.tag == 1 {
            controllerPackage.setReceiverName(receiverName: text)
        } else if textField.tag == 2 {
            controllerPackage.setReceiverPhone(receiverPhone: text)
        }
    }
}

extension NewOrder4ViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let text = textView.text else { return }
        controllerPackage.setComments(comments: text)
    }
}


