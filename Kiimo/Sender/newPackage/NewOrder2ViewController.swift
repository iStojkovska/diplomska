//
//  NewOrder2ViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/18/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

class NewOrder2ViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var pickupAddressTextField: UITextField!
    @IBOutlet weak var additionalInfoTextField: UITextField!
    
    var controllerPackage: Package!
    private var locationManager: CLLocationManager = CLLocationManager()
    private var selectedTextField: UITextField = UITextField()
    private var viewMoved: CGFloat = 0.0
    
    private var selectedButton: Int = 1 {
        didSet {
            changeSelectedButton(new: selectedButton, old: oldValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationManager()
        initView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let pageViewController = self.parent as? PageViewController else { return }
        guard let nextVC = pageViewController.getViewController(index: 2) as? NewOrder3ViewController else { return }
        nextVC.controllerPackage = self.controllerPackage
    }
    
    public func setPackageFromPreviousVC(package: Package) {
        controllerPackage.setName(name: package.getName())
        controllerPackage.setSize(size: package.getSize())
        controllerPackage.setDeliverDate(deliverDate: package.getDeliverDate())
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func initView() {
        selectedButtonUI(button: self.view.viewWithTag(1) as! UIButton)
        for i in 2...4 {
            if let button: UIButton = view.viewWithTag(i) as? UIButton {
                unselectedButtonUI(button: button)
            }
        }
        additionalInfoTextField.delegate = self
        pickupAddressTextField.layer.cornerRadius = 10
        additionalInfoTextField.layer.cornerRadius = 10
    }
    
    private func setupLocationManager() {
        print(">>>>>>>>>>> setupLocationManager <<<<<<<<<<<<<<<<")
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 250; // meters, set according to the required value.
        locationManager.startUpdatingLocation()
    }
    
    private func changeSelectedButton (new: Int, old: Int) {
        if new != old {
            let newlySelected = self.view.viewWithTag(new) as! UIButton
            let previouslySelected = self.view.viewWithTag(old) as! UIButton
            selectedButtonUI(button: newlySelected)
            unselectedButtonUI(button: previouslySelected)
        }
    }
    
    private func selectedButtonUI(button: UIButton) {
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(red: 151.0/255, green: 151.0/255, blue: 151.0/255, alpha: 0.35).cgColor
        button.backgroundColor = UIColor(red: 176/255, green: 27/255, blue: 37/255, alpha: 1.0)
        button.layer.cornerRadius = 8
        button.setTitleColor(UIColor.white, for: .normal)
        UIManagmentService.sharedInstance.addShadowOnView(onView: button, shadowDirection: .down)
    }
    
    private func unselectedButtonUI(button: UIButton) {
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(red: 151.0/255, green: 151.0/255, blue: 151.0/255, alpha: 0.35).cgColor
        button.backgroundColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
        button.layer.cornerRadius = 8
        button.setTitleColor(UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.73), for: .normal)
        UIManagmentService.sharedInstance.addShadowOnView(onView: button, shadowDirection: .down)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            if self.view.frame.origin.y == 0 {
                if selectedTextField.frame.maxY + 16 > frame.origin.y {
                    viewMoved = selectedTextField.frame.maxY + 16 - frame.origin.y
                    self.view.frame.origin.y -= viewMoved
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y += viewMoved
        }
    }
    
    @IBAction func exchangeTypeButtonTapped(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            print("Call me tappped")
            controllerPackage.setExchangeType(exchangeType: .call)
        case 2:
            print("Wait outside tapped")
            controllerPackage.setExchangeType(exchangeType: .waitOutside)
        case 3:
            print("Reception tapped")
            controllerPackage.setExchangeType(exchangeType: .reception)
        case 4:
            print("Other tapped")
            controllerPackage.setExchangeType(exchangeType: .other)
        default:
            print("Call me tappped")
            controllerPackage.setExchangeType(exchangeType: .call)
        }
        selectedButton = sender.tag
    }
    
    @IBAction func pickupTextFieldEditingDidBegin(_ sender: UITextField) {
        sender.resignFirstResponder()
        let autoCompleteVC: GMSAutocompleteViewController = GMSAutocompleteViewController()
        autoCompleteVC.delegate = self
        let filter: GMSAutocompleteFilter = GMSAutocompleteFilter()
        filter.country = "MK"
        autoCompleteVC.autocompleteFilter = filter
        self.present(autoCompleteVC, animated: true, completion: nil)
    }
    
}

extension NewOrder2ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(">>>>>>>> locationManager didChangeAuthorization <<<<<<<<")
        
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            //CAMERA SHOULD UPDATE
            guard let latitude = locationManager.location?.coordinate.latitude else { return }
            guard let longitude = locationManager.location?.coordinate.longitude else {return }
            
            mapView.camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15.0)
            manager.showsBackgroundLocationIndicator = true
        } else {
            mapView.camera = GMSCameraPosition.camera(withLatitude: Constants.SKOPJE_LAT, longitude: Constants.SKOPJE_LONG, zoom: 15.0)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            
            mapView.camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15.0)
            manager.showsBackgroundLocationIndicator = true
            
        } else {
            mapView.camera = GMSCameraPosition.camera(withLatitude: Constants.SKOPJE_LAT, longitude: Constants.SKOPJE_LONG, zoom: 15.0)
        }
    }
}


extension NewOrder2ViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15.0)
        let marker: GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude))
        marker.map = self.mapView
        self.mapView.camera = camera
        pickupAddressTextField.text = place.name
        controllerPackage.setStartAddress(startAddress: place.name)
        controllerPackage.setStartLocation(startLocation: Location(start: "\(place.coordinate.latitude)", end: "\(place.coordinate.longitude)"))
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Autocoplete ended with error: \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NewOrder2ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 5 {
            guard let text = textField.text else { return }
            controllerPackage.setAdditinalInfo(additinalInfo: text)
        }
    }
}
