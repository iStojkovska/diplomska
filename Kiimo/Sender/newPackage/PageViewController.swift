//
//  PageViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/18/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

protocol PageViewControllerPackageDelegate: class {
    func newPackageAdded(package: Package)
}

class PageViewController: UIPageViewController {
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [instantiateVC(name: "NewOrder1ViewController"),
                instantiateVC(name: "NewOrder2ViewController"),
                instantiateVC(name: "NewOrder3ViewController"),
                instantiateVC(name: "NewOrder4ViewController")]
    }()
    
    private var pageControl: UIPageControl = UIPageControl()
    
    weak var packageDelegate: PageViewControllerPackageDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        if let firstVC = orderedViewControllers.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        configuratePageControl()
    }
    
    private func instantiateVC (name: String) -> UIViewController {
        return UIStoryboard(name: "sender", bundle: nil).instantiateViewController(withIdentifier: name)
    }
    
    private func configuratePageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: Constants.screenWidth, height: 50))
        pageControl.numberOfPages = orderedViewControllers.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor(named: "Primary1")
        pageControl.pageIndicatorTintColor = UIColor.black
        pageControl.currentPageIndicatorTintColor = UIColor(named: "Primary1")
        self.view.addSubview(pageControl)
    }
    
    public func getViewController(index: Int) -> UIViewController {
        return orderedViewControllers[index]
    }

}

extension PageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return orderedViewControllers.count + 1
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = orderedViewControllers.index(where: {$0 == viewController}) ?? 0
        if currentIndex <= 0 {
            return nil
        }
        return orderedViewControllers[currentIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = orderedViewControllers.index(where: {$0 == viewController}) ?? 0

        if currentIndex >= (orderedViewControllers.count - 1) {
            return nil
        }
        
        return orderedViewControllers[currentIndex + 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentVC = pageViewController.viewControllers![0]
        pageControl.currentPage = orderedViewControllers.index(where: {$0 == pageContentVC})!
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        let index: Int = orderedViewControllers.index(where: {$0 == pendingViewControllers[0]}) ?? 0
        if index == orderedViewControllers.count {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
