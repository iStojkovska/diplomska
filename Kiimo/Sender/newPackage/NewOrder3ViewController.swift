//
//  NewOrder3ViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/18/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class NewOrder3ViewController: UIViewController {
    
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var suggestedPriceTextField: UITextField!
    
    var controllerPackage: Package!
    private var locationManager: CLLocationManager = CLLocationManager()
    private var selectedTextField: UITextField = UITextField()
    private var viewMoved: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        setupLocationManager()
        suggestedPriceTextField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let pageViewController = self.parent as? PageViewController else { return }
        guard let nextVC = pageViewController.getViewController(index: 3) as? NewOrder4ViewController else { return }
        nextVC.controllerPackage = self.controllerPackage
    }
    
    private func initView() {
        destinationTextField.layer.cornerRadius = 10
        suggestedPriceTextField.layer.cornerRadius = 10
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            if self.view.frame.origin.y == 0 {
                if selectedTextField.frame.maxY + 16 > frame.origin.y {
                    viewMoved = selectedTextField.frame.maxY + 16 - frame.origin.y
                    self.view.frame.origin.y -= viewMoved
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y += viewMoved
        }
    }
    
    private func setupLocationManager() {
        print(">>>>>>>>>>> setupLocationManager <<<<<<<<<<<<<<<<")
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 250; // meters, set according to the required value.
        locationManager.startUpdatingLocation()
    }
    
    
    @IBAction func destinationTextFieldEditingDidBegin(_ sender: UITextField) {
        sender.resignFirstResponder()
        let autoCompleteVC: GMSAutocompleteViewController = GMSAutocompleteViewController()
        autoCompleteVC.delegate = self
        let filter: GMSAutocompleteFilter = GMSAutocompleteFilter()
        filter.country = "MK"
        autoCompleteVC.autocompleteFilter = filter
        self.present(autoCompleteVC, animated: true, completion: nil)
    }
    
}

extension NewOrder3ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(">>>>>>>> locationManager didChangeAuthorization <<<<<<<<")
        
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            //CAMERA SHOULD UPDATE
            guard let latitude = locationManager.location?.coordinate.latitude else { return }
            guard let longitude = locationManager.location?.coordinate.longitude else {return }
            
            mapView.camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15.0)
            manager.showsBackgroundLocationIndicator = true
        } else {
            mapView.camera = GMSCameraPosition.camera(withLatitude: Constants.SKOPJE_LAT, longitude: Constants.SKOPJE_LONG, zoom: 15.0)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            
            mapView.camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15.0)
            manager.showsBackgroundLocationIndicator = true
            
        } else {
            mapView.camera = GMSCameraPosition.camera(withLatitude: Constants.SKOPJE_LAT, longitude: Constants.SKOPJE_LONG, zoom: 15.0)
        }
    }
}

extension NewOrder3ViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15.0)
        let marker: GMSMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude))
        marker.map = self.mapView
        self.mapView.camera = camera
        destinationTextField.text = place.name
        controllerPackage.setEndAddress(endAddress: place.name)
        controllerPackage.setEndLocation(endLocation: Location(start: "\(place.coordinate.latitude)", end: "\(place.coordinate.longitude)"))
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Autocoplete ended with error: \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NewOrder3ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 6 {
            guard let text = textField.text else { return }
            controllerPackage.setPrice(price: text)
        }
    }
}
