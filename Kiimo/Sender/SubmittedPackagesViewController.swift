//
//  SubmittedPackagesViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/17/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class SubmittedPackagesViewController: UIViewController {
    
    @IBOutlet weak var topNavigationBar: NavigationBar!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var addNewPackageButton: UIButton!
    
    private var scrollView: UIScrollView = UIScrollView()
    private var submittedPckages: [Package]  = []
    private var originY: CGFloat = 0
    
    private var selectedId: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollView()
        errorLabel.layer.opacity = 0
        addNewPackageButton.addTarget(self, action: #selector(addNewPackageButtonTapped), for: .touchUpInside)
        getAllSubmittedPackages()
    }
    
    private func initScrollView() {
        scrollView = UIScrollView(frame: CGRect(x: 0, y: topNavigationBar.frame.height, width: self.view.frame.width, height: self.view.frame.height - topNavigationBar.frame.height))
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat(submittedPckages.count * 82))
        scrollView.showsVerticalScrollIndicator = false
        scrollView.bounces = false
        self.view.addSubview(scrollView)
        
        self.view.bringSubviewToFront(addNewPackageButton)
    }
    
    private func getAllSubmittedPackages() {
        NetworkManager.sharedInstance.getSenderActivePackages(id: User.sharedInstance.getUserId()) { (packageList, error) in
            
            if let packages = packageList {
                if packages.count > 0 {
                    self.submittedPckages = packages
                    self.scrollView.translatesAutoresizingMaskIntoConstraints = true
                    
                    for i in 0 ..< packages.count {
                        if Date(timeIntervalSince1970: TimeInterval(packages[i].getDeliverDate())) > Date() {
                            let packageView: PackageView = PackageView(frame: CGRect(x: 0, y: self.originY, width: self.view.frame.width, height: 88), package: packages[i])
                            packageView.tag = i
                            packageView.delegate = self
                            self.originY += 88
                            self.scrollView.addSubview(packageView)
                            self.scrollView.contentSize.height += 88
                        }
                    }
                    UIManagmentService.sharedInstance.animateLabels(viewTag: 0, view: self.scrollView)
                    
                } else {
                    if self.errorLabel.layer.opacity == 0 {
                        UIView.animate(withDuration: 0.2, animations: {
                            self.errorLabel.layer.opacity = 1
                        })
                    } else {
                        UIView.animate(withDuration: 0.2, animations: {
                            self.errorLabel.layer.opacity = 0
                        }, completion: { (_) in
                            self.errorLabel.layer.opacity = 1
                        })
                    }
                }
            } else {
                guard let err = error else {
                    print("Cannot get optional value of error")
                    return
                }
                if let statusCode = err["statusCode"], let errMessage = err["errMsg"] {
                    print("=====CANNOT GET ALL SUBMITTED PACKAGES ========")
                    print("WITH STATUS CODE \(statusCode) AND MESSAGE \(errMessage)")
                }
                if self.errorLabel.layer.opacity == 0 {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.errorLabel.layer.opacity = 1
                    })
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "submittedPackageDetailsSegue" {
            if let packageDetailsVC = segue.destination as? PackageDetailsViewController {
                packageDetailsVC.setSelectedPackageID(id: selectedId, code: nil)
            }
        } else if segue.identifier == "addNewPackageSegue" {
            if let pageVC = segue.destination as? PageViewController {
                pageVC.packageDelegate = self
            }
        }
    }
    
    @objc private func addNewPackageButtonTapped() {
        performSegue(withIdentifier: "addNewPackageSegue", sender: nil)
    }
}

extension SubmittedPackagesViewController: PackageViewDelegate {
    func packageTapped(id: Int) {
        selectedId = id
        performSegue(withIdentifier: "submittedPackageDetailsSegue", sender: nil)
    }
}

extension SubmittedPackagesViewController: PageViewControllerPackageDelegate {
    func newPackageAdded(package: Package) {
        let packageView: PackageView = PackageView(frame: CGRect(x: 0, y: originY, width: self.view.frame.width, height: 88), package: package)
        self.originY += 88
        packageView.layer.opacity = 1
        self.scrollView.contentSize.height += 88
        self.scrollView.addSubview(packageView)
    }
}
