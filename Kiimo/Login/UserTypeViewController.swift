//
//  UserTypeViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/11/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class UserTypeViewController: UIViewController {
    
    @IBOutlet weak var wantToSendButton: UIButton!
    @IBOutlet weak var wantToDeliverButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.title = ""
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        initView()
    }
    
    private func initView() {
        wantToSendButton.layer.cornerRadius = 10
        wantToDeliverButton.layer.cornerRadius = 10
        
        wantToDeliverButton.titleLabel?.font = UIFont.bold16
        wantToSendButton.titleLabel?.font = UIFont.bold16
        
        errorLabel.font = UIFont.regular12
        errorLabel.layer.opacity = 0
    }
    
    private func showErrorLabel(error: String) {
        errorLabel.text = error
        if errorLabel.layer.opacity == 0 {
            UIView.animate(withDuration: 0.3, animations: {
                self.errorLabel.layer.opacity = 1
            })
        }
    }
    
    
    @IBAction func senderOptionTapped(_ sender: UIButton) {
        
        User.sharedInstance.setType(userType: .sender)
        NetworkManager.sharedInstance.createUser(user: User.sharedInstance) { (model, error) in
            
            if let userModel = model {
                User.sharedInstance.updateUser(user: userModel)
                UserDefaults.standard.set(User.sharedInstance.getUserId(), forKey: "userId")
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                self.performSegue(withIdentifier: "senderRegistration", sender: nil)
                
            } else {
                self.showErrorLabel(error: "Registration failed")
            }
            
        }
    }
    

}
