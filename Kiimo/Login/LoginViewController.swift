//
//  LoginViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/9/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginButtonTopConstaint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        initView()
    }
    
    private func initView() {
        loginButton.layer.cornerRadius = 10
        passwordTextField.layer.cornerRadius = 10
        emailTextField.layer.cornerRadius = 10
        
        loginButton.titleLabel?.font = UIFont.bold16
        passwordTextField.font = UIFont.regular16
        emailTextField.font = UIFont.regular16
        registerButton.titleLabel?.font = UIFont.regular16
        
        errorLabel.font = UIFont.regular12
        errorLabel.layer.opacity = 0
    }
    
    private func showErrorLabel(error: String) {
        if errorLabel.layer.opacity == 0 {
            UIView.animate(withDuration: 0.3, animations: {
                self.errorLabel.text = error
                self.loginButtonTopConstaint.constant += self.errorLabel.frame.height
                self.view.layoutIfNeeded()
            }) { (_) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.errorLabel.layer.opacity = 1
                })
            }
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.errorLabel.layer.opacity = 0
            }) { (_) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.errorLabel.text = error
                    self.errorLabel.layer.opacity = 1
                })
            }
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        
        if (emailTextField.text?.isEmpty)! {
            showErrorLabel(error: "Please enter your email")
        } else if (passwordTextField.text?.isEmpty)! {
            showErrorLabel(error: "Please enter your password")
        } else {
            NetworkManager.sharedInstance.login(email: emailTextField.text!, password: passwordTextField.text!) { (model, error) in
                
                if let userModel = model {
                    User.sharedInstance.updateUser(user: userModel)
                    UserDefaults.standard.set(User.sharedInstance.getUserId(), forKey: "userId")
                    if userModel.getType() == .sender {
                        self.navigationController?.setNavigationBarHidden(true, animated: true)
                        self.performSegue(withIdentifier: "senderLogin", sender: nil)
                    } else if userModel.getType() == .deliverer {
                        self.navigationController?.setNavigationBarHidden(true, animated: true)
                        self.performSegue(withIdentifier: "delivererLogin", sender: nil)
                    }
                } else {
                    self.showErrorLabel(error: "Your input is not valid")
                }
                
            }
        }
        
    }
    

}
