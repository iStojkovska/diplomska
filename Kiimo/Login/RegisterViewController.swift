//
//  RegisterViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/9/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    private var selectedTextField: UITextField = UITextField()
    private var viewMoved: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.title = ""
        self.navigationController?.navigationBar.shadowImage = UIImage()
        initView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func initView() {
        
        registerButton.layer.cornerRadius = 10
        nameTextField.layer.cornerRadius = 10
        emailTextField.layer.cornerRadius = 10
        passwordTextField.layer.cornerRadius = 10
        confirmPasswordTextField.layer.cornerRadius = 10

        registerButton.titleLabel?.font = UIFont.bold16
        nameTextField.font = UIFont.regular16
        emailTextField.font = UIFont.regular16
        passwordTextField.font = UIFont.regular16
        confirmPasswordTextField.font = UIFont.regular16
        
        nameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        errorLabel.font = UIFont.regular12
        errorLabel.layer.opacity = 0
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            if self.view.frame.origin.y == 0 {
                if selectedTextField.frame.maxY + 16 > frame.origin.y {
                    viewMoved = selectedTextField.frame.maxY + 16 - frame.origin.y
                    self.view.frame.origin.y -= viewMoved
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y += viewMoved
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    
    private func showErrorLabel(error: String) {
        
        if errorLabel.layer.opacity == 0 {
            UIView.animate(withDuration: 0.3, animations: {
                self.errorLabel.text = error
            }) { (_) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.errorLabel.layer.opacity = 1
                })
            }
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.errorLabel.layer.opacity = 0
            }) { (_) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.errorLabel.text = error
                    self.errorLabel.layer.opacity = 1
                })
            }
        }
    }
    
    private func emailValidation(email: String?) -> Bool {
        guard email != nil else { return false }
        
        let regEx = "([a-zA-Z0-9+._%-+]{1,256})(@)([a-zA-Z0-9][a-zA-Z0-9-]{0,64})(()(.)([a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})()+)"
        
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    @IBAction func registerButtonTapped(_ sender: UIButton) {
        
        if !(nameTextField.text?.isEmpty)! && !(emailTextField.text?.isEmpty)! && !(passwordTextField.text?.isEmpty)! && !(confirmPasswordTextField.text?.isEmpty)! {
            if passwordTextField.text != confirmPasswordTextField.text {
                showErrorLabel(error: "Your passwords don't match")
            } else {
                User.sharedInstance.setFullName(fullName: nameTextField.text!)
                User.sharedInstance.setEmail(email: emailTextField.text!)
                User.sharedInstance.setPassword(password: passwordTextField.text!)
                performSegue(withIdentifier: "userTypeSegue", sender: nil)
            }
        } else {
            if (nameTextField.text?.isEmpty)! {
                showErrorLabel(error: "Please enter your name")
            } else if !emailValidation(email: emailTextField.text) {
                showErrorLabel(error: "Please enter your valid email")
            } else if (passwordTextField.text?.isEmpty)! {
                showErrorLabel(error: "Please enter your password")
            } else if (confirmPasswordTextField.text?.isEmpty)! {
                showErrorLabel(error: "Please econfirm your password")
            }
            
        }
        
    }
    
    
}
