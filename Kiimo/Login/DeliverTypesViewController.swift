//
//  DeliverTypesViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/12/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class DeliverTypesViewController: UIViewController {
    
    @IBOutlet weak var lSizeButton: UIButton!
    @IBOutlet weak var mSizeButton: UIButton!
    @IBOutlet weak var sSizeButton: UIButton!
    @IBOutlet weak var toDateTextField: UITextField!
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    private var selectedTextField: UITextField = UITextField()
    private var viewMoved: CGFloat = 0.0
    
    private var numberOfSelectedSizes: Int = 0
    private var selectedSizes: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.title = ""
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        initCheckbox(button: sSizeButton)
        initCheckbox(button: mSizeButton)
        initCheckbox(button: lSizeButton)
        initView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    private func initCheckbox(button: UIButton) {
        button.setImage(#imageLiteral(resourceName: "uncheckedBox"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "checkedBox"), for: .selected)
        button.addTarget(self, action: #selector(toggleCheckboxSelection(button:)), for: .touchUpInside)
    }
    
    private func initView() {
        toDateTextField.font = UIFont.regular12
        fromDateTextField.font = UIFont.regular12
        toDateTextField.layer.cornerRadius = 10
        fromDateTextField.layer.cornerRadius = 10
        
        doneButton.layer.cornerRadius = 10
        doneButton.titleLabel?.font = UIFont.bold16
        
        errorLabel.font = UIFont.regular12
        errorLabel.layer.opacity = 0
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            if self.view.frame.origin.y == 0 {
                if wrapperView.frame.maxY + 16 > frame.origin.y {
                    viewMoved = wrapperView.frame.maxY + 16 - frame.origin.y
                    self.view.frame.origin.y -= viewMoved
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0{
            self.view.frame.origin.y += viewMoved
        }
    }
    
    @objc private func toggleCheckboxSelection(button: UIButton) {
        button.isSelected = !button.isSelected
        
        var value: Int!
        if button == sSizeButton {
            value = 1
        } else if button == mSizeButton {
            value = 2
        } else if button == lSizeButton {
            value = 3
        }
        
        if button.isSelected {
            numberOfSelectedSizes += 1
            selectedSizes.append(value)
        } else {
            numberOfSelectedSizes -= 1
            selectedSizes.removeAll(where: {$0 == value})
        }
        print("======== NUMBER OF SELECTED SIZES: \(numberOfSelectedSizes) ========")
        print("======== SELECTED SIZES: \(selectedSizes) ==========")
    }
    
    @objc private func datePickerChanged (datePicker: UIDatePicker) {
    
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        selectedTextField.text = formatter.string(from: datePicker.date)
        
        if selectedTextField == fromDateTextField {
            User.sharedInstance.setPreferredDateStart(start: Int64((datePicker.date.timeIntervalSince1970).rounded()))
        } else if selectedTextField == toDateTextField {
            User.sharedInstance.setPreferredDateEnd(end: Int64((datePicker.date.timeIntervalSince1970).rounded()))
            
        }
    }
    
    
    @IBAction func dateTextFieldDidBeginEditing(_ sender: UITextField) {
        selectedTextField = sender
        let datePicker: UIDatePicker = UIDatePicker()
        sender.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePickerChanged(datePicker:)), for: .valueChanged)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func doneButtonTapped(_ sender: UIButton) {
        
        if !(toDateTextField.text?.isEmpty)! && !(fromDateTextField.text?.isEmpty)! {
            if numberOfSelectedSizes > 0 {
                if selectedSizes.count == 3 {
                    User.sharedInstance.setPreferredSize(size: "sml")
                } else if selectedSizes.count == 2 {
                    if selectedSizes.contains(1) {
                        if selectedSizes.contains(2) {
                            User.sharedInstance.setPreferredSize(size: "sm")
                        } else {
                            User.sharedInstance.setPreferredSize(size: "sl")
                        }
                    } else {
                        User.sharedInstance.setPreferredSize(size: "ml")
                    }
                } else if selectedSizes.contains(1) {
                    User.sharedInstance.setPreferredSize(size: "s")
                } else if selectedSizes.contains(2) {
                    User.sharedInstance.setPreferredSize(size: "m")
                } else if selectedSizes.contains(3) {
                    User.sharedInstance.setPreferredSize(size: "l")
                }
                
                NetworkManager.sharedInstance.createUser(user: User.sharedInstance) { (model, error) in
                    if let userModel = model {
                        User.sharedInstance.updateUser(user: userModel)
                        UserDefaults.standard.set(User.sharedInstance.getUserId(), forKey: "userId")
                        self.navigationController?.setNavigationBarHidden(true, animated: true)
                        self.performSegue(withIdentifier: "delivererRegistration", sender: nil)
                        
                    } else {
                        UIManagmentService.sharedInstance.showErrorLabel(type: .updateError, errorLabel: self.errorLabel, text: "Registration failed")
                    }
                }
            } else {
                UIManagmentService.sharedInstance.showErrorLabel(type: .updateError, errorLabel: errorLabel, text: "Please choose a size.")
            }
        } else {
            UIManagmentService.sharedInstance.showErrorLabel(type: .showError, errorLabel: errorLabel, text: "Please choose available time.")
        }
        
    }
    
}
