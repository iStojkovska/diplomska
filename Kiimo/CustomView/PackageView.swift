//
//  PackageView.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/28/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

protocol PackageViewDelegate: class {
    func packageTapped(id: Int)
}

class PackageView: UIView {
    
    private var package: Package = Package()
    private var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()
    
    weak var delegate: PackageViewDelegate?
    
    init(frame: CGRect, package: Package) {
        super.init(frame: frame)
        self.package = package
        self.backgroundColor = .white
        self.addBottomBorderWithColor(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7300000191), width: 1)
        self.layer.opacity = 0
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        initLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initLayout() {
        
        let packageImageView: UIImageView = UIImageView()
        packageImageView.contentMode = .scaleAspectFill
        
        if !package.getImageUrl().isEmpty {
            packageImageView.sd_setImage(with: URL(string: package.getImageUrl()), placeholderImage: #imageLiteral(resourceName: "activePachages"), options: [], completed: nil)
        } else {
            packageImageView.image = #imageLiteral(resourceName: "activePachages")
        }
        let packagePriceLabel: UILabel = UILabel()
        let attributetString: NSMutableAttributedString = NSMutableAttributedString(
            string: "$ ",
            attributes: [NSAttributedString.Key.font: UIFont.regular26, NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6901960784, green: 0.1058823529, blue: 0.1450980392, alpha: 1)])
        attributetString.append(NSAttributedString(
            string: "\(package.getPrice())",
            attributes: [NSAttributedString.Key.font: UIFont.regular26, NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6901960784, green: 0.1058823529, blue: 0.1450980392, alpha: 1)]))
        packagePriceLabel.attributedText = attributetString
        packagePriceLabel.textAlignment = .center
        
        let packageNameLabel: UILabel = UILabel()
        packageNameLabel.text = package.getName()
        packageNameLabel.font = UIFont.bold18
        packageNameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7300000191)
        packageNameLabel.textAlignment = .left
        packageNameLabel.numberOfLines = 1
        
        let deliverDateLabel: UILabel = UILabel()
        deliverDateLabel.font = UIFont.regular14
        deliverDateLabel.textAlignment = .left
        deliverDateLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7300000191)
        deliverDateLabel.numberOfLines = 0
        deliverDateLabel.sizeToFit()
        deliverDateLabel.text = "Delivered by \(UIManagmentService.sharedInstance.getDeliverTimeFormat(deliverDate: package.getDeliverDate()))"
        
        self.addSubview(packageImageView)
        self.addSubview(packagePriceLabel)
        self.addSubview(packageNameLabel)
        self.addSubview(deliverDateLabel)
        
        self.addGestureRecognizer(tapGestureRecognizer)
        
        packageImageView.translatesAutoresizingMaskIntoConstraints = false
        packagePriceLabel.translatesAutoresizingMaskIntoConstraints = false
        packageNameLabel.translatesAutoresizingMaskIntoConstraints = false
        deliverDateLabel.translatesAutoresizingMaskIntoConstraints = false
        
        packageImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16).isActive = true
        packageImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 4).isActive = true
        packageImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        packageImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        packagePriceLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16).isActive = true
        packagePriceLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        packageNameLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 16).isActive = true
        packageNameLabel.leadingAnchor.constraint(equalTo: packageImageView.trailingAnchor, constant: 16).isActive = true
        packageNameLabel.trailingAnchor.constraint(equalTo: packagePriceLabel.leadingAnchor, constant: -16).isActive = true
        
        deliverDateLabel.leadingAnchor.constraint(equalTo: packageNameLabel.leadingAnchor).isActive = true
        deliverDateLabel.trailingAnchor.constraint(equalTo: packageNameLabel.trailingAnchor).isActive = true
        deliverDateLabel.topAnchor.constraint(equalTo: packageNameLabel.bottomAnchor, constant: 4).isActive = true
        
    }
    
    func viewBecomeVisible (completion: @escaping(_ completed: Bool) -> Void) {
        UIView.animate(withDuration: 0.15, delay: 0.01, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.layer.opacity = 1
        }) { (_) in
            completion(true)
        }
    }
    
    @objc private func viewTapped() {
        delegate?.packageTapped(id: package.getId())
    }
    
}

extension UIView {
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
}
