//
//  NavigationBar.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/17/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

protocol NavigationBarDelegate: class {
    func sideManuTapped()
}

@IBDesignable class NavigationBar: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    weak var delegate: NavigationBarDelegate?
    
    @IBInspectable private var title: String? {
        
        didSet{
            titleLabel.text = title
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup()  {
        
        let bundle = Bundle(for: NavigationBar.self)
        bundle.loadNibNamed("NavigationBar", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth, .flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
    }
    
    @IBAction func sideManuClicked(_ sender: UIButton) {
        delegate?.sideManuTapped()
    }
    
}
