//
//  Constants.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/14/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    let statusAnimationDuration = 0.3

    static let screenWidth: CGFloat = UIScreen.main.bounds.width
    static let screenHeight: CGFloat = UIScreen.main.bounds.height
    
    static let SKOPJE_LAT: Double = 41.9960
    static let SKOPJE_LONG: Double = 21.4316
    
    static let apiKey: String = "FEEE3235626C079DA26FF0CCBACBA430F809CDF5A8DC7358AEE52F05AE2E6ED0"
}
