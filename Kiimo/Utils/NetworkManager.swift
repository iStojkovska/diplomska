//
//  NetworkManager.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/14/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation

let baseUrl: String = "https://ivana-project.herokuapp.com/api"

class NetworkManager {
    
    static let sharedInstance = NetworkManager()
    
    private func errObjcParser(_ statusCode: Int?, _ errMsg: String?) -> [String: Any]{
        return ["statusCode": statusCode ?? "", "errMsg": errMsg ?? ""]
    }
    
    func createUser(user: User, completion: @escaping (_ response: User?, _ errObj: [String: Any]?) -> Void ) {
        let url: String = baseUrl + "/user/create"
        var parametres: [String: Any]
        if !user.getPreferredSize().isEmpty, let preferredDate = user.getPreferredDate() {
            parametres = ["email": user.getEmail(), "password": user.getPassword(), "fullName": user.getFullName(), "type": user.getType().rawValue, "preferredSize": user.getPreferredSize(), "preferredDate": ["start": preferredDate.start, "end": preferredDate.end]]
        } else {
            parametres = ["email": user.getEmail(), "password": user.getPassword(), "fullName": user.getFullName(), "type": user.getType().rawValue]
        }
        RequestService.sharedInstance.postRequest(urlString: url, params: parametres) { (response, rspData, rspJSON, error, statusCode) in
            if let data = rspData{
                do {
                    let decoder: JSONDecoder = JSONDecoder()
                    let model = try decoder.decode(User.self, from: data)
                    completion(model, nil)
                } catch let error as NSError {
                    print("error from decoder: \(error)")
                    completion(nil, self.errObjcParser(nil, "Error from decoding"))
                }
            } else {
                completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
            }
        }
    }
    
    func login(email: String, password: String, completion: @escaping (_ userModel: User?, _ errObj: [String: Any]?) -> Void) {
        let url: String = baseUrl + "/user/login?email=\(email)&password=\(password)"
        RequestService.sharedInstance.getRequest(urlString: url, params: nil) { (response, rspData, rspJSON, error, statusCode) in
            
            if let data = rspData {
                do {
                    let decoder: JSONDecoder = JSONDecoder()
                    let model = try decoder.decode(User.self, from: data)
                    completion(model, nil)
                } catch let error as NSError {
                    print("error from decoding: \(error)")
                    completion(nil, self.errObjcParser(nil, "Error from decoding"))
                }
            } else {
                completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
            }
        }
    }
    
    func getUserInfo(id: String, completion: @escaping (_ userModel: User?, _ errObj: [String: Any]?) -> Void) {
        let url: String = baseUrl + "/user/get?id=\(id)"
        RequestService.sharedInstance.getRequest(urlString: url, params: nil) { (reponse, rspData, rspJSON, error, statusCode) in
            if let data = rspData {
                do {
                    let decoder: JSONDecoder = JSONDecoder()
                    let model = try decoder.decode(User.self, from: data)
                    completion(model, nil)
                } catch let error as NSError {
                    print("error from decoding: \(error)")
                    completion(nil, self.errObjcParser(nil, "Error from decoding"))
                }
            } else {
                completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
            }
        }
        
    }
    
    func createPackage(package: Package, completion: @escaping (_ response: Bool, _ errObj: [String: Any]? ) -> Void) {
        let url: String = baseUrl + "/package/create"
        let parametres: [String: Any] = [
            "startLocation":["start": package.getStartLocation().start, "end": package.getStartLocation().end],
            "endLocation": ["start": package.getEndLocation().start, "end": package.getEndLocation().end],
            "startAddress": package.getStartAddress(),
            "endAddress": package.getEndAddress(),
            "name": package.getName(),
            "price": package.getPrice(),
            "senderId": User.sharedInstance.getUserId(),
            "receiverName": package.getReceiverName(),
            "receiverPhone": package.getReceiverPhone(),
            "comments": package.getComments(),
            "deliverDate": package.getDeliverDate(),
            "size": package.getSize(),
            "additinalInfo": package.getAdditinalInfo(),
            "exchangeType": package.getExchangeType().rawValue]
        RequestService.sharedInstance.postRequest(urlString: url, params: parametres) { (response, rspData, rspJSON, error, statusCode) in
            
            if response != nil {
                if let json = rspJSON {
                    if let done = (json as! [String: Any])["done"] as? Bool {
                        completion(done, nil)
                    } else {
                        completion(false, self.errObjcParser(nil, "Fail getting value from json"))
                    }
                } else {
                    completion(false, self.errObjcParser(statusCode, error?.localizedDescription))
                }
            } else {
                completion(false, self.errObjcParser(statusCode, error?.localizedDescription))
            }
        }
    }
    
    func getSenderActivePackages(id: Int, completion: @escaping (_ packages: [Package]?, _ errObj: [String: Any]?) -> Void) {
        let url: String = baseUrl + "/sender/active?id=\(id)"
        RequestService.sharedInstance.getRequest(urlString: url, params: nil) { (response, rspData, rspJSON, error, statusCode) in
            
            if let data = rspData {
                do {
                    let decoder: JSONDecoder = JSONDecoder()
                    let model = try decoder.decode([Package].self, from: data)
                    
                    if let jsonPackages = rspJSON as? [[String: Any]] {
                        if jsonPackages.count > 0 {
                            for i in 0 ... (jsonPackages.count - 1) {
                                model[i].setStartLocation(startLocation: self.getStartEndLocationFromJSON(json: jsonPackages[i]).0)
                                model[i].setEndLocation(endLocation: self.getStartEndLocationFromJSON(json: jsonPackages[i]).1)
                            }
                        }
                    }
                    
                    completion(model, nil)
                } catch let error {
                    print("erroooor: \(error)")
                    completion(nil, self.errObjcParser(nil, error.localizedDescription))
                }
            } else {
                print("ERROR --> \(String(describing: error)) <-- ERROR")
                completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
            }
        }
    }
    
    func getAllDelivererPackages(max: Int, page: Int, completion: @escaping (_ packages: [Package]?, _ errObj: [String: Any]?) -> Void) {
        let url: String = baseUrl + "/deliverer/all?max=\(max)&page=\(page)"
        RequestService.sharedInstance.getRequest(urlString: url, params: nil) { (response, rspData, rspJSON, error, statusCode) in
            
            if let data = rspData {
                do {
                    let decoder: JSONDecoder = JSONDecoder()
                    let model: [Package] = try decoder.decode([Package].self, from: data)
                    completion(model, nil)
                } catch {
                    completion(nil, self.errObjcParser(nil, "Error from decoding"))
                }
            } else {
                print("ERROR --> \(String(describing: error)) <-- ERROR")
                completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
            }
        }
    }
    
    func acceptPackage(delivererId: Int, packageId: Int, senderId: String, completion: @escaping (_ response: String?, _ errObj: [String: Any]?) -> Void) {
        let url: String = baseUrl + "/deliverer/package/accept"
        let parametres: [String: Any] = ["delivererId": delivererId, "packageId": packageId, "senderId": senderId]
        RequestService.sharedInstance.postRequest(urlString: url, params: parametres) { (response, rspData, rspJSON, error, statusCode) in
            
            if response != nil {
                if let json = rspJSON {
                    if let code = (json as! [String: Any])["code"] as? String {
                        completion(code, nil)
                    } else {
                        completion(nil, self.errObjcParser(nil, "Fail getting value from json"))
                    }
                } else {
                    completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
                }
            } else {
                completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
            }
        }
    }
    
    func getInTransitPackages(id: Int, completion: @escaping (_ packages: PackageInTransit?, _ errObj: [String: Any]?) -> Void) {
        let url: String = baseUrl + "/packages/intransit?id=\(id)"
        RequestService.sharedInstance.getRequest(urlString: url, params: nil) { (response, rspData, rspJSON, error, statusCode) in
            
            if let data = rspData{
                do {
                    let decoder: JSONDecoder = JSONDecoder()
                    let model = try decoder.decode(PackageInTransit.self, from: data)
                    completion(model, nil)
                } catch let error {
                    print("erroooor: \(error)")
                    completion(nil, self.errObjcParser(nil, "Error from decoding"))
                }
            } else {
                print("ERROR --> \(String(describing: error)) <-- ERROR")
                completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
            }
        }
    }
    
    func getPackageDetails(id: Int, completion: @escaping (_ package: Package?,_ errObj: [String: Any]?) -> Void) {
        let url: String = baseUrl + "/package/get?id=\(id)"
        RequestService.sharedInstance.getRequest(urlString: url, params: nil) { (response, rspData, rspJSON, error, statusCode) in
            if let data = rspData{
                do {
                    let decoder: JSONDecoder = JSONDecoder()
                    let model = try decoder.decode(Package.self, from: data)
                    
                    if let jsonPackage = rspJSON as? [String: Any] {
                        model.setStartLocation(startLocation: self.getStartEndLocationFromJSON(json: jsonPackage).0)
                        model.setEndLocation(endLocation: self.getStartEndLocationFromJSON(json: jsonPackage).1)
                        
                    }
                    completion(model, nil)
                } catch {
                    completion(nil, self.errObjcParser(nil, "Error from decoding"))
                }
            } else {
                print("ERROR --> \(String(describing: error)) <-- ERROR")
                completion(nil, self.errObjcParser(statusCode, error?.localizedDescription))
            }
            
        }
    }
    
    func packageDelivered(id: Int, confirmationCode: String, rating: Int, comment: String, completion: @escaping (_ response: Bool, _ errObj: [String: Any]?) -> Void) {
        let url: String = baseUrl + "/package/delivered"
        let parametres: [String: Any] = ["packageId": id, "confirmationCode": confirmationCode, "rating": rating, "comment": comment]
        RequestService.sharedInstance.postRequest(urlString: url, params: parametres) { (response, rspData, rspJSON, error, statusCode) in
            
            if response != nil {
                if let json = rspJSON {
                    if let done = (json as! [String: Any])["done"] as? Bool {
                        completion(done, nil)
                    } else {
                        completion(false, self.errObjcParser(nil, "Fail getting value from json"))
                    }
                } else {
                    completion(false, self.errObjcParser(statusCode, error?.localizedDescription))
                }
            } else {
                completion(false, self.errObjcParser(statusCode, error?.localizedDescription))
            }
            
        }
        
    }
    
    private func getStartEndLocationFromJSON (json: [String: Any]) -> (Location, Location) {
        
        var startlocation: Location = Location()
        var endlocation: Location = Location()
        
        if let startLocation = json["startLocation"] as? [String: Any] {
            guard let startLat = startLocation["start"] else { return (startlocation, endlocation) }
            guard let startLng = startLocation["end"] else { return (startlocation, endlocation) }
            
            startlocation = Location(start: "\(startLat)", end: "\(startLng)")
            
        }
        if let endLocation = json["endLocation"] as? [String: Any] {
            guard let endLat = endLocation["start"] else { return (startlocation, endlocation) }
            guard let endLng = endLocation["end"] else { return (startlocation, endlocation) }
            
            endlocation = Location(start: "\(endLat)", end: "\(endLng)")
        }
        return (startlocation, endlocation)
        
    }
}
