//
//  RequestService.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/14/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class RequestService {
    
    static let sharedInstance: RequestService = RequestService()
    private var headers: HTTPHeaders = HTTPHeaders()
    private var debugLogger: Bool = true
    
    public func getRequest(
        urlString: String,
        params: [String: Any]?,
        completion: @escaping(_ rsp: DataResponse<Any>?, _ rspData: Data?, _ rspJSON: Any?, _ error: Error?, _ statusCode: Int?) -> Void)
    {
        self.baseGetRequest(urlString: urlString, params: params) { (response, rspData, rspJSON, error, statusCode) in
            completion(response, rspData, rspJSON, error, statusCode)
        }
    }
    
    public func postRequest(
        urlString: String,
        params: [String: Any]?,
        completion: @escaping(_ rsp: DataResponse<Any>?, _ rspData: Data?, _ rspJSON: Any?, _ error: Error?, _ statusCode: Int?) -> Void)
    {
        self.basePostRequest(urlString: urlString, params: params) { (response, rspData, rspJSON, error, statusCode) in
            completion(response, rspData, rspJSON, error, statusCode)
        }
    }

    private func baseGetRequest(
        urlString: String,
        params: [String: Any]?,
        completion: @escaping(_ rsp: DataResponse<Any>?, _ rspData: Data?, _ rspJSON: Any?, _ error: Error?, _ statusCode: Int?) -> Void)
    {
        headers.removeAll()
        headers = [
            "api-key": Constants.apiKey
        ]
        
        if debugLogger {
            print("------ GET ------")
            print("urlString: \(urlString)")
            print("params: \(String(describing: params))")
            print("headers: \(headers)")
        }
        
        Alamofire.request(urlString, method: .get, parameters: params, headers: headers)
            .validate()
            .responseJSON { response in
                if self.debugLogger {
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments)
                        print("GET JSON Response: \(json)")
                    } catch let error as NSError {
                        print(error)
                    }
                    print("-----------------")
                }
                switch response.result {
                case .success:
                    guard let rspData = response.data else {
                        if self.debugLogger {
                            print("FAILED TO RETRIVE DATA FROM API CALL")
                        }
                        completion(nil, nil, nil, response.error, response.response?.statusCode)
                        return
                    }
                    completion(response, rspData, response.result.value, response.error, response.response?.statusCode)
                case .failure(let error):
                    completion(nil, nil, nil, error, response.response?.statusCode ?? error._code)
                }
        }
    }
    
    private func basePostRequest(
        urlString: String,
        params: [String: Any]?,
        completion: @escaping(_ rsp: DataResponse<Any>?, _ rspData: Data?, _ rspJSON: Any?, _ error: Error?, _ statusCode: Int?) -> Void)
    {
        headers.removeAll()
        headers = [
            "api-key": Constants.apiKey
        ]
        
        if debugLogger {
            print("------ POST ------")
            print("urlString: \(urlString)")
            print("params: \(String(describing: params))")
            print("headers: \(headers)")
        }
        
        Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { response in
                if self.debugLogger {
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String: Any]
                        print("POST JSON Response: \(String(describing: json))")
                    } catch let error as NSError {
                        print(error)
                    }
                    print("------------------")
                }
                switch response.result {
                case .success:
                    guard let rspData = response.data else {
                        if self.debugLogger {
                            print("FAILED TO RETRIVE DATA FROM API CALL")
                        }
                        completion(nil, nil, nil, response.error, (response.response?.statusCode)!)
                        return
                    }
                    completion(response, rspData, response.result.value, response.error, (response.response?.statusCode)!)
                case .failure(let error):
                    completion(nil, nil, nil, error, response.response?.statusCode ?? error._code)
                }
        }
    }
}
