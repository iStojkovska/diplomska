//
//  UIFont.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/9/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    static let regular10: UIFont = UIFont(name: "HelveticaNeue", size: 10)!
    static let regular12: UIFont = UIFont(name: "HelveticaNeue", size: 12)!
    static let regular14: UIFont = UIFont(name: "HelveticaNeue", size: 14)!
    static let regular16: UIFont = UIFont(name: "HelveticaNeue", size: 16)!
    static let regular18: UIFont = UIFont(name: "HelveticaNeue", size: 18)!
    static let regular20: UIFont = UIFont(name: "HelveticaNeue", size: 20)!
    static let regular26: UIFont = UIFont(name: "HelveticaNeue", size: 26)!
    
    static let bold10: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 10)!
    static let bold12: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 12)!
    static let bold14: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 14)!
    static let bold16: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 16)!
    static let bold18: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 18)!
    static let bold20: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 20)!
    static let bold26: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 26)!
    static let bold46: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 46)!
}
