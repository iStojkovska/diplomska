//
//  UIManagementService.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 11/22/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import Foundation
import UIKit

class UIManagmentService {
    
    static let sharedInstance: UIManagmentService = UIManagmentService()
    
    enum shadowDirection {
        case up
        case down
        case center
        case left
        case right
    }
    
    enum errorType {
        case showError
        case updateError
    }
    
    public func addShadowOnView(onView: UIView, shadowDirection: shadowDirection) {
        onView.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.8).cgColor
        onView.layer.shadowOpacity = 0.2
        onView.layer.shadowOffset = CGSize(width: shadowDirection == .right ? 3 : shadowDirection == .left ? -3 : 0, height: shadowDirection == .up ? -3 : shadowDirection == .down ? 3 : 0)
        onView.layer.masksToBounds = false
    }
    
    public func removeAllSubview (view: UIView) {
        for subView in view.subviews {
            subView.removeFromSuperview()
        }
    }
    
    public func animateLabels(viewTag: Int, view: UIView) {
        if viewTag < view.subviews.count {
            if view.subviews[viewTag].isKind(of: PackageView.self) {
                (view.subviews[viewTag] as! PackageView).viewBecomeVisible { (completed) in
                    if completed {
                        self.animateLabels(viewTag: viewTag + 1, view: view)
                    }
                }
            } else { self.animateLabels(viewTag: viewTag + 1, view: view) }
        }
    }
    
    public func getDeliverTimeFormat(deliverDate: Int64) -> String {
        
        
        let date: Date = Date(timeIntervalSince1970: TimeInterval(deliverDate))
        let calendar: Calendar = Calendar.current
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        
        if calendar.isDateInToday(date) {
            dateFormatter.dateFormat = "HH:mm"
            return "today at \(dateFormatter.string(from: date))"
        } else if calendar.isDateInYesterday(date) {
            dateFormatter.dateFormat = "HH:mm"
            return "yesterday at \(dateFormatter.string(from: date))"
        } else {
            dateFormatter.dateFormat = "EEE, dd MMM HH:mm"
            return dateFormatter.string(from: date)
        }
    }
    
    public func getExchangeTimeFormat(exchangeType: ExchangeType) -> String {
        switch exchangeType {
        case .call:
            return "Call me 5 min before the arrival"
        case .other:
            return "Other"
        case .reception:
            return "Pickup at the reception"
        case .waitOutside:
            return "I will be waiting outside"
        }
    }
    
    public func showErrorLabel(type: errorType, errorLabel: UILabel, text: String? ) {
        if type == .updateError{
            if errorLabel.layer.opacity == 0 {
                if text != nil {
                    errorLabel.text = text!
                }
                UIView.animate(withDuration: 0.2, animations: {
                    errorLabel.layer.opacity = 1
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    errorLabel.layer.opacity = 0
                    if text != nil {
                        errorLabel.text = text!
                    }
                }, completion: { (_) in
                    errorLabel.layer.opacity = 1
                })
            }
        } else {
            if errorLabel.layer.opacity == 0 {
                UIView.animate(withDuration: 0.2, animations: {
                    if text != nil {
                        errorLabel.text = text!
                    }
                    errorLabel.layer.opacity = 1
                })
            }
        }
        
    }
    
}

