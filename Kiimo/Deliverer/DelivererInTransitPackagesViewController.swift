//
//  DelivererInTransitPackagesViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 12/5/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class DelivererInTransitPackagesViewController: UIViewController {

    @IBOutlet weak var topNavigationBar: NavigationBar!
    @IBOutlet weak var errorLabel: UILabel!
    
    private var scrollView: UIScrollView = UIScrollView()
    private var packagesInTransit: PackageInTransit = PackageInTransit()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollView()
        errorLabel.layer.opacity = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if scrollView.subviews.count > 0 {
            for subview in scrollView.subviews {
                subview.removeFromSuperview()
            }
        }
        getPackagesInTransit()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        errorLabel.layer.opacity = 0
    }
    
    private func initScrollView() {
        scrollView = UIScrollView(frame: CGRect(x: 0, y: topNavigationBar.frame.height, width: self.view.frame.width, height: self.view.frame.height - topNavigationBar.frame.height))
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat(packagesInTransit.packages.count * 82))
        scrollView.showsVerticalScrollIndicator = false
        scrollView.bounces = false
        self.view.addSubview(scrollView)
    }
    
    private func getPackagesInTransit() {
        NetworkManager.sharedInstance.getInTransitPackages(id: User.sharedInstance.getUserId()) { (packagesInTransit, error) in
            
            if let packages = packagesInTransit?.packages {
                if packages.count > 0 {
                    self.packagesInTransit = packagesInTransit!
                    self.scrollView.translatesAutoresizingMaskIntoConstraints = true
                    self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat(packages.count * 88))
                    
                    var originY: CGFloat = 0
                    for i in 0 ..< packages.count {
                        let packageView: PackageView = PackageView(frame: CGRect(x: 0, y: originY, width: self.view.frame.width, height: 88), package: packages[i])
                        packageView.tag = i
                        packageView.delegate = self
                        originY += 88
                        self.scrollView.addSubview(packageView)
                    }
                    UIManagmentService.sharedInstance.animateLabels(viewTag: 0, view: self.scrollView)
                } else {
                    UIManagmentService.sharedInstance.showErrorLabel(type: .updateError, errorLabel: self.errorLabel, text: nil)
                }
            } else {
                guard let err = error else {
                    print("Cannot get optional value of error")
                    return
                }
                if let statusCode = err["statusCode"], let errMessage = err["errMsg"] {
                    print("=====CANNOT GET ALL DELIVERER'S PACKAGES IN TRANSIT ========")
                    print("WITH STATUS CODE \(statusCode) AND MESSAGE \(errMessage)")
                }
                UIManagmentService.sharedInstance.showErrorLabel(type: .showError, errorLabel: self.errorLabel, text: nil)
            }
            
        }
    }
    
}

extension DelivererInTransitPackagesViewController: PackageViewDelegate {
    func packageTapped(id: Int) {
        if let packageDetailsVC = UIStoryboard(name: "sender", bundle: nil).instantiateViewController(withIdentifier: "PackageDetailsViewController") as? PackageDetailsViewController {
            packageDetailsVC.setSelectedPackageID(id: id, code: nil)
            packageDetailsVC.buttonTitle = "DELIVER"
            self.present(packageDetailsVC, animated: true, completion: nil)
        }
    }
}
