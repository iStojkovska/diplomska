//
//  ActivePackagesViewController.swift
//  Kiimo
//
//  Created by Ivana Stojkovska on 12/5/18.
//  Copyright © 2018 Ivana Stojkovska. All rights reserved.
//

import UIKit

class ActivePackagesViewController: UIViewController {

    @IBOutlet weak var topNavigationBar: NavigationBar!
    @IBOutlet weak var errroLabel: UILabel!
    
    private var scrollView: UIScrollView = UIScrollView()
    private var activePackages: [Package] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initScrollView()
        errroLabel.layer.opacity = 0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAllActivePackages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        errroLabel.layer.opacity = 0
    }
    
    private func initScrollView() {
        scrollView = UIScrollView(frame: CGRect(x: 0, y: topNavigationBar.frame.height, width: self.view.frame.width, height: self.view.frame.height - topNavigationBar.frame.height))
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat(activePackages.count * 82))
        scrollView.showsVerticalScrollIndicator = false
        scrollView.bounces = false
        self.view.addSubview(scrollView)
    }
    
    private func getAllActivePackages() {
        NetworkManager.sharedInstance.getAllDelivererPackages(max: 20, page: 1) { (packageList, error) in
            
            if let packages = packageList {
                if packages.count > 0 {
                    self.activePackages = packages
                    self.scrollView.translatesAutoresizingMaskIntoConstraints = true
                    var originY: CGFloat = 0
                    for i in 0 ..< packages.count {
                        if Date(timeIntervalSince1970: TimeInterval(packages[i].getDeliverDate())) > Date() {
                            let packageView: PackageView = PackageView(frame: CGRect(x: 0, y: originY, width: self.view.frame.width, height: 88), package: packages[i])
                            packageView.tag = i
                            packageView.delegate = self
                            originY += 88
                            self.scrollView.contentSize.height += 88
                            self.scrollView.addSubview(packageView)
                        }
                    }
                    UIManagmentService.sharedInstance.animateLabels(viewTag: 0, view: self.scrollView)
                } else {
                    UIManagmentService.sharedInstance.showErrorLabel(type: .showError, errorLabel: self.errroLabel, text: nil)
                }
                
                
            } else {
                guard let err = error else {
                    print("Cannot get optional value of error")
                    return
                }
                if let statusCode = err["statusCode"], let errMessage = err["errMsg"] {
                    print("=====CANNOT GET ALL DELIVERER PACKAGES ========")
                    print("WITH STATUS CODE \(statusCode) AND MESSAGE \(errMessage)")
                }
                UIManagmentService.sharedInstance.showErrorLabel(type: .showError, errorLabel: self.errroLabel, text: nil)
            }
            
        }
    }
    
}

extension ActivePackagesViewController: PackageViewDelegate {
    func packageTapped(id: Int) {
        
        if let packageDetailsVC = UIStoryboard(name: "sender", bundle: nil).instantiateViewController(withIdentifier: "PackageDetailsViewController") as? PackageDetailsViewController {
            packageDetailsVC.setSelectedPackageID(id: id, code: nil)
            packageDetailsVC.buttonTitle = "BID"
            packageDetailsVC.delegate = self
            self.present(packageDetailsVC, animated: true, completion: nil)
        }
    }
}

extension ActivePackagesViewController: PackageDetailsViewControllerDelegate {
    func bidButtonTapped() {
        self.tabBarController?.selectedIndex = 1
    }
    
    
}
